import Aside from './Aside.vue'
import Callout from './Callout.vue'
import Footer from './Footer.vue'
import Header from './Header.vue'
import Sidebar from './Sidebar.vue'
import Switch from './Switch.vue'

export {
  Aside,
  Callout,
  Footer,
  Header,
  Sidebar,
  Switch
}

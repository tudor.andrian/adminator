export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'house-outline'
    },
    {
      title: true,
      name: 'Tools',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Email',
      url: '/scopes/email',
      icon: 'email-outline'
    },
    {
      name: 'Compose',
      url: '/scopes/compose',
      icon: 'share-outline'
    },
    {
      name: 'Chat',
      url: '/scopes/chat',
      icon: 'chat'
    },
    {
      name: 'Charts',
      url: '/scopes/charts',
      icon: 'chart-finance'
    },
    {
      name: 'Forms',
      url: '/scopes/forms',
      icon: 'lead-pencil'
    },
    {
      name: 'UI Elements',
      url: '/scopes/ui-elements',
      icon: 'palette'
    },
    {
      name: 'Tables',
      url: '/scopes/tables',
      icon: 'view-list'
    },
    {
      name: 'Maps',
      url: '/scopes/maps',
      icon: 'map'
    },
    {
      name: 'Pages',
      url: '/scopes/pages',
      icon: 'file-multiple'
    },
    {
      name: 'Multiple Levels',
      url: '/scopes/multiple-levels',
      icon: 'format-list-numbers'
    }
  ]
}

import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

//Scopes
import Dashboard from '@/views/Dashboard'
import Scopes from '@/views/scopes/Scopes'
import Calendar from '@/views/scopes/Calendar'
import Charts from '@/views/scopes/Charts'
import Chat from '@/views/scopes/Chat'
import Compose from '@/views/scopes/Compose'
import Email from '@/views/scopes/Email'
import Forms from '@/views/scopes/Forms'
import UIelements from '@/views/scopes/UI-elements'
import Tables from '@/views/scopes/Tables'
import BasicTable from '@/views/scopes/tables/Basic-table'
import DataTable from '@/views/scopes/tables/Data-table'
import Maps from '@/views/scopes/Maps'
import GoogleMap from '@/views/scopes/maps/Google-map'
import VectorMap from '@/views/scopes/maps/Vector-map'
import Pages from '@/views/scopes/Pages'
import P404 from '@/views/scopes/pages/404'
import P500 from '@/views/scopes/pages/500'
import SignIn from '@/views/scopes/pages/Sign-in'
import SignUp from '@/views/scopes/pages/Sign-up'
import MultipleLevels from '@/views/scopes/Multiple-levels'
import MenuItem from '@/views/scopes/multiple-levels/Menu-item'
import MenuItem2 from '@/views/scopes/multiple-levels/Menu-item2'
import MenuItemSecond from '@/views/scopes/multiple-levels/Menu-item-second'
import MenuItemSec from '@/views/scopes/multiple-levels/menu-item/menu-item-sec'
import MenuItemSec2 from '@/views/scopes/multiple-levels/menu-item/menu-item-sec2'

// Pages
import Login from '@/views/pages/Login'

import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0}),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'scopes',
          name: 'Scopes',
          redirect: 'scopes/index',
          component: {
            render (c) { return c('router-view')}
          },
          children: [
            {
              path: 'index',
              name: 'Index',
              component: Scopes
            },
            {
              path: 'email',
              name: 'Email',
              component: Email
            },
            {
              path: 'compose',
              name: 'Compose',
              component: Compose
            },
            {
              path: 'calendar',
              name: 'Calendar',
              component: Calendar
            },
            {
              path: 'chat',
              name: 'Chat',
              component: Chat
            },
            {
              path: 'charts',
              name: 'Charts',
              component: Charts
            },
            {
              path: 'forms',
              name: 'Forms',
              component: Forms
            },
            {
              path: 'ui-elements',
              name: 'UIelements',
              component: UIelements
            }
          ]
        },
        {
          path: 'tables',
          name: 'Tables',
          redirect: 'tables/index',
          component: {
            render (c) {return c('router-view')}
          },
          childern: [
            {
              path: 'index',
              name: 'Index',
              component: Tables
            },
            {
              path: 'basic-table',
              name: 'BasicTable',
              component: BasicTable
            },
            {
              path: 'data-tables',
              name: 'DataTables',
              component: DataTable
            }
          ]
        },
        {
          path: 'maps',
          name: 'Maps',
          redirect: 'maps/index',
          component: {
            render (c) {return c('router-view')}
          },
          children: [
            {
              path: 'index',
              name: 'Index',
              component: Maps
            },
            {
              path: 'google-map',
              name: 'GoogleMap',
              component: GoogleMap
            },
            {
              path: 'vector-map',
              name: 'VectorMap',
              component: VectorMap
            }
          ]
        },
        {
          path: 'pages',
          name: 'Pages',
          redirect: 'pages/index',
          component: {
            render (c) { return c('router-view')}
          },
          children: [
            {
              path: 'index',
              name: 'Index',
              component: Pages
            },
            {
              path: '404',
              name: 'P404',
              component: P404
            },
            {
              path: '500',
              name: 'P500',
              component: P500
            },
            {
              path: 'sign-in',
              name: 'SignIn',
              component: SignIn
            },
            {
              path: 'sign-up',
              name: 'SignUp',
              component: SignUp
            }
          ]
        },
        {
          path: 'multiple-levels',
          name: 'MultipleLevels',
          redirect: 'multiple-levels/index',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'index',
              name: 'Index',
              component: MultipleLevels
            },
            {
              path: 'menu-item',
              name: 'MenuItem',
              component: MenuItem
            },
            {
              path: 'menu-item2',
              name: 'MenuItem2',
              component: MenuItem2
            },
            {
              path: 'menu-item-second',
              name: 'MenuItemSecond',
              redirect: 'menu-item-second/index',
              component: {
                render (c) { return c('router-view')}
              },
              children: [
                {
                  path: 'index',
                  name: 'Index',
                  component: MenuItemSecond
                },
                {
                  path: 'menu-item-sec',
                  name: 'MenuItemSec',
                  component: MenuItemSec
                },
                {
                  path: 'menu-item-sec2',
                  name: 'MenuItemSec2',
                  component: MenuItemSec2
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
    // {
    //   path: '/',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // }
  ]
})
